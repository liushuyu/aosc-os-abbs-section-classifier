import pickle

from train import filter_desc

def predict(tfidf, classifier, text):
    transformed = tfidf.transform([filter_desc(text)])
    return classifier.predict(transformed)


def main():
    print('Loading...')
    with open('tfidf_pickle.dat', 'rb') as f:
        tfidf = pickle.load(f)
    with open('tag_classifier_pickle.dat', 'rb') as f:
        classifier = pickle.load(f)
    print('... done')
    while True:
        text = input('> ')
        if text == "exit":
            break
        print(predict(tfidf, classifier, text)[0])
    

if __name__ == "__main__":
    main()
