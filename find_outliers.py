import logging
import sys
from collections import defaultdict

def read_input_data(file):
    """Helper function to get training data"""
    logging.info('Opening file %s for reading input', file)
    input_file = open(file, 'r')
    labels = defaultdict(int)
    for line in input_file:
        tokens = line.split('\t', 1)
        label = tokens[0].strip()
        labels[label] += 1
    return labels


def pretty_print(d):
    def truncate_or_pad(name):
        length = len(name)
        if length > field_size:
            name = name[:field_size]
        return name + (' ' * (field_size - length))
    field_size = 30
    d = sorted(d.items(), key=lambda x: x[1])
    print('-' * field_size)
    for k, v in d:
        if k == 'section':
            continue
        print('%s%s' % (truncate_or_pad(k), v))


def main():
    pretty_print(read_input_data(sys.argv[1]))

if __name__ == "__main__":
    main()
