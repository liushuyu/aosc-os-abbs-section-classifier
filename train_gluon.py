import subprocess

from sklearn.model_selection import train_test_split
from train import load_data


def main():
    OUTPUT_FILE = 'spec_data.gluon.train.txt'
    TEST_FILE = 'spec_data.gluon.test.txt'
    print('Preparing data...')
    samples = load_data()
    train, test = train_test_split(samples.values, train_size=0.9, test_size=0.1)
    with open(OUTPUT_FILE, 'wt') as f:
        for entry in train:
            f.write('%s, %s\n' % (entry[0], entry[1]))
    with open(TEST_FILE, 'wt') as f:
        for entry in test:
            f.write('%s, %s\n' % (entry[0], entry[1]))


if __name__ == "__main__":
    main()
