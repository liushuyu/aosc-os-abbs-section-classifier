import subprocess

from sklearn.model_selection import train_test_split
from train import load_data


def main():
    OUTPUT_FILE = 'spec_data.star.train.txt'
    TEST_FILE = 'spec_data.star.test.txt'
    train_args = ['starspace', 'train', '-trainFile', OUTPUT_FILE, '-model',
            'tag_classifier.starspace', '-epoch', '8', '-lr', '0.001']
    test_args = ['starspace', 'test', '-testFile', TEST_FILE, '-model',
            'tag_classifier.starspace']
    print('Preparing data...')
    samples = load_data()
    train, test = train_test_split(samples.values, train_size=0.9, test_size=0.1)
    with open(OUTPUT_FILE, 'wt') as f:
        for entry in train:
            f.write('%s __label__%s\n' % (entry[1], entry[0]))
    with open(TEST_FILE, 'wt') as f:
        for entry in test:
            f.write('%s __label__%s\n' % (entry[1], entry[0]))
    print('Training using Starspace...')
    subprocess.check_call(train_args)
    print('Testing...')
    subprocess.check_call(test_args)


if __name__ == "__main__":
    main()
