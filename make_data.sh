#!/bin/bash
if [[ "x$1" == "x" ]]; then
  echo 'Please specify the folder'
  exit 1
fi

if ! test -d "$1" ; then
  echo "$1 is not a folder"
  exit 1
fi

echo 'Preparing data for the classifier...'
echo -ne "section\tdescription\n" > spec_data.tsv

find "$1" -name 'defines' -type f -exec bash -ec "source \"{}\"; [ ! -z \"\$PKGSEC\" ] && echo -ne \"\$PKGSEC\t\$PKGNAME \$PKGDES\n\" >> spec_data.tsv" ';'
