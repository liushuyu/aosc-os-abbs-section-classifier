import sklearn
import pickle
import nltk
import re
import pandas
import logging
import random

from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LogisticRegression
from sklearn.multiclass import OneVsRestClassifier
from sklearn.metrics import accuracy_score
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import FunctionTransformer

from skl2onnx import convert_sklearn
from skl2onnx.common.data_types import StringTensorType


def filter_descs(texts: list) -> list:
    outputs = []
    for text in texts:
        outputs.append(filter_desc(text))
    return outputs


def filter_desc(text) -> str:
    space_subst = r'[/(){}\[\]\|,;:\.-]'
    remove = r'[^0-9a-z #+_]'
    stopwords = set(nltk.corpus.stopwords.words('english'))

    output = re.sub(space_subst, ' ', text.lower())
    output = re.sub(remove, '', output)
    output = ' '.join([i for i in output.split() if i and i not in stopwords])

    return output.strip()


def calculate_tfidf(train, test, dump_path):
    tfidf = TfidfVectorizer(ngram_range=(1, 3), max_df=0.9, min_df=3)
    trained = tfidf.fit_transform(train)
    tested = tfidf.transform(test)
    return trained, tested


def load_data(data_path='spec_data.tsv'):
    logging.info('Loading data from dataset...')
    samples = pandas.read_csv(data_path, sep='\t', dtype='str')
    samples['description'] = [filter_desc(i) for i in samples['description']]
    return samples


def none_transform(self):
    return self


def main():
    print('Loading sample data...')
    samples = load_data()
    tag_train, tag_test, desc_train, desc_test = train_test_split(
        samples['section'].values, samples['description'].values, train_size=0.9, test_size=0.1)
    print('Training...')
    tag_classifier = OneVsRestClassifier(LogisticRegression(
        solver='lbfgs', C=5, penalty='l2', random_state=0), -1)
    tag_classifier.transform = none_transform
    model = Pipeline(steps=[
        # ('preprocess', FunctionTransformer(filter_descs, validate=False)),
        ('transform', TfidfVectorizer(ngram_range=(1, 3), max_df=0.9, min_df=3)),
        ('classify', tag_classifier)
    ])
    tag_trained = filter_descs(tag_train)
    desc_trained = filter_descs(desc_train)
    model.fit_transform(desc_trained, tag_trained)
    tag_test_guess = model.predict(desc_test)
    print('Accuracy: %s' % accuracy_score(tag_test, tag_test_guess))
    idx = random.randint(0, desc_train.shape[0])
    prediction = model.predict([desc_trained[idx]])[0]
    print('Example: %s: %s' % (desc_trained[idx], prediction))
    # export to ONNX
    print('Exporting to ONNX...')
    initial_type = [('input', StringTensorType([1, 1]))]
    model_onnx = convert_sklearn(
        model, initial_types=initial_type, target_opset=10)
    with open("tfidf.onnx", "wb") as f:
        f.write(model_onnx.SerializeToString())
    print('Done.')


if __name__ == "__main__":
    main()
